---
home: true
heroImage: /img/logo.png
heroText: 365UAV
tagline: 无人机知识库
# actionText: 立刻进入 →
# actionLink: /web/
bannerBg: none # auto => 网格纹背景(有bodyBgImg时无背景)，默认 | none => 无 | '大图地址' | background: 自定义背景样式       提示：如发现文本颜色不适应你的背景时可以到palette.styl修改$bannerTextColor变量

features: # 可选的
  - title: 基础知识
    details: 了解基础，轻松入门
    link: /base/ # 可选
    imgUrl: /img/uav.png # 可选
  - title: 法律法规
    details: 了解最新法律法规，安全无忧使用无人机
    link: /law/
    imgUrl: /img/law.png
  - title: 场景应用
    details: 打开你的视野，发现神奇的应用
    link: /scene/
    imgUrl: /img/scene.png
  - title: 保险保障
    details: 保护他人，保护自己
    link: /insure/ # 可选
    imgUrl: /img/ins.png #
  - title: 执照培训
    details: 专业合规，使用无忧
    link: /license/
    imgUrl: /img/lic.png
  - title: 保养维修
    details: 好用耐用，远离麻烦
    link: /repair/
    imgUrl: /img/repair.png
# 文章列表显示方式: detailed 默认，显示详细版文章列表（包括作者、分类、标签、摘要、分页等）| simple => 显示简约版文章列表（仅标题和日期）| none 不显示文章列表
postList: none
# simplePostListLength: 10 # 简约版文章列表显示的文章数量，默认10。（仅在postList设置为simple时生效）
hideRightBar: true # 是否隐藏右侧边栏
---
## 📚关于无人机知识库

这是一个知识库网站，主要收录一些无人机相关基础知识，方便您入门了解无人机，使用无人机。当前以多旋翼相关为主，后期会逐渐丰富。 ( •̀ ω •́ )✧

:::tip
文章内容仅是我个人的小总结，资历尚浅，如有误还请指正。
:::

<!-- <div style="width: 300px;height: 300px;position: fixed;bottom: 0;left: 0;z-index: 1;">
  <script type="text/javascript" src="//rf.revolvermaps.com/0/0/8.js?i=5e4x5w8cxxb&m=0&c=ff0000&cr1=ffffff&f=arial&l=33&bv=80" async="async"></script>
</div> -->

<!-- 小熊猫 

<img src="/img/panda-waving.png" class="panda no-zoom" style="width: 130px;height: 115px;opacity: 0.8;margin-bottom: -4px;padding-bottom:0;position: fixed;bottom: 0;left: 0.5rem;z-index: 1;">
-->
## 🎨用爱发电

如果您觉得本网站内容对您有帮助，或者是想支持我们继续开发，您可以通过如下任意方式支持我们：

1.分享本网站给您的朋友，共同学习无人机⭐️<br/>
2.添加我的微信，向我提出您的宝贵的意见 🎇<br/>
3.通过以下二维码进行捐款，请我们喝一杯咖啡 ☕️<P>
非常感谢！ ❤️<br>


<img src="/365uav/img/donate.png">


## :email: 联系
相关联系方式（添加烦请注明来意）：
- QQ: <a :href="qqUrl" class='qq'>{{ QQ }}</a>
- Email:  <a href="mailto:2920648907@qq.com">2920648907@qq.com</a>
- Wechat:<br/>
<img src="/365uav/img/wechat.png">

## 致谢
感谢给予支持的朋友，您的支持是我们前进的动力 🎉

<script>
  export default {
    data(){
      return {
        QQ: '2920648907',
        qqUrl: `tencent://message/?uin=${this.QQ}&Site=&Menu=yes`
      }
    },
    mounted(){
      const flag =  navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i);
      if(flag){
        this.qqUrl = `mqqwpa://im/chat?chat_type=wpa&uin=${this.QQ}&version=1&src_type=web&web_src=oicqzone.com`
      }
    }
  }
</script>