(window.webpackJsonp=window.webpackJsonp||[]).push([[34],{320:function(t,a,s){"use strict";s.r(a);var r={data(){return{QQ:"2920648907",qqUrl:`tencent://message/?uin=${this.QQ}&Site=&Menu=yes`}},mounted(){navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i)&&(this.qqUrl=`mqqwpa://im/chat?chat_type=wpa&uin=${this.QQ}&version=1&src_type=web&web_src=oicqzone.com`)}},e=s(7),i=Object(e.a)(r,(function(){var t=this,a=t._self._c;return a("ContentSlotsDistributor",{attrs:{"slot-key":t.$parent.slotKey}},[a("h2",{attrs:{id:"📚关于无人机知识库"}},[a("a",{staticClass:"header-anchor",attrs:{href:"#📚关于无人机知识库"}},[t._v("#")]),t._v(" 📚关于无人机知识库")]),t._v(" "),a("p",[t._v("这是一个知识库网站，主要收录一些无人机相关基础知识，方便您入门了解无人机，使用无人机。当前以多旋翼相关为主，后期会逐渐丰富。 ( •̀ ω •́ )✧")]),t._v(" "),a("div",{staticClass:"custom-block tip"},[a("p",{staticClass:"custom-block-title"},[t._v("提示")]),t._v(" "),a("p",[t._v("文章内容仅是我个人的小总结，资历尚浅，如有误还请指正。")])]),t._v(" "),a("h2",{attrs:{id:"🎨用爱发电"}},[a("a",{staticClass:"header-anchor",attrs:{href:"#🎨用爱发电"}},[t._v("#")]),t._v(" 🎨用爱发电")]),t._v(" "),a("p",[t._v("如果您觉得本网站内容对您有帮助，或者是想支持我们继续开发，您可以通过如下任意方式支持我们：")]),t._v(" "),a("p",[t._v("1.分享本网站给您的朋友，共同学习无人机⭐️"),a("br"),t._v("\n2.添加我的微信，向我提出您的宝贵的意见 🎇"),a("br"),t._v("\n3.通过以下二维码进行捐款，请我们喝一杯咖啡 ☕️"),a("P",[t._v("\n非常感谢！ ❤️"),a("br")]),t._v(" "),a("img",{attrs:{src:"/365uav/img/donate.png"}})],1),a("h2",{attrs:{id:"联系"}},[a("a",{staticClass:"header-anchor",attrs:{href:"#联系"}},[t._v("#")]),t._v(" ✉️ 联系")]),t._v(" "),a("p",[t._v("相关联系方式（添加烦请注明来意）：")]),t._v(" "),a("ul",[a("li",[t._v("QQ: "),a("a",{staticClass:"qq",attrs:{href:t.qqUrl}},[t._v(t._s(t.QQ))])]),t._v(" "),a("li",[t._v("Email:  "),a("a",{attrs:{href:"mailto:2920648907@qq.com"}},[t._v("2920648907@qq.com")])]),t._v(" "),a("li",[t._v("Wechat:"),a("br"),t._v(" "),a("img",{attrs:{src:"/365uav/img/wechat.png"}})])]),t._v(" "),a("h2",{attrs:{id:"致谢"}},[a("a",{staticClass:"header-anchor",attrs:{href:"#致谢"}},[t._v("#")]),t._v(" 致谢")]),t._v(" "),a("p",[t._v("感谢给予支持的朋友，您的支持是我们前进的动力 🎉")])])}),[],!1,null,null,null);a.default=i.exports}}]);